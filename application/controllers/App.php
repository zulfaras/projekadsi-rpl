<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {


	public function index()
	{

		if ($this->session->userdata('username') == "") {
			redirect('app/login');
		}
		$data = array(
			'konten' => 'home',
            'judul' => 'Dashboard',
		);
		$this->load->view('v_index', $data);
	}

	function profiluser($id){
		if ($_POST==NULL) {
			$d = $this->db->query("SELECT * from datapelanggan where ID_pelanggan ='$id'")->row();
			$data = array(
				'rw' => $d,
				'konten' => 'profiluser',
	            'judul' => 'Data Profil',
			);
			$this->load->view('v_index', $data);
		} else {
			$data = array(
				'nama' => $this->input->post('nama'),
				'alamat' => $this->input->post('alamat'),
				'Tanggal_bayar' => $this->input->post('Tanggal_bayar'),
				'Status_bayar' => $this->input->post('Status_bayar'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),

			);

			$this->db->where('ID_pelanggan', $id);
			$this->db->update('datapelanggan', $data);
			?>
			<script type="text/javascript">
				alert('Berhasil ubah data !');
				window.location='<?php echo base_url()?>app/profiluser/<?php echo $id; ?>';
			</script>
			<?php
		}
	}

	function profiladmin($id){
		if ($_POST==NULL) {
			$d = $this->db->query("SELECT * from user where id_user='$id'")->row();
			$data = array(
				'rw' => $d,
				'konten' => 'profiladmin',
	            'judul' => 'Data Profil',
			);
			$this->load->view('v_index', $data);
		} else {
			$data = array(
				'nama' => $this->input->post('nama'),
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
			);

			$this->db->where('id_user', $id);
			$this->db->update('user', $data);
			?>
			<script type="text/javascript">
				alert('Berhasil ubah data !');
				window.location='<?php echo base_url()?>app/profiladmin/<?php echo $id; ?>';
			</script>
			<?php
		}
	}

	function ubahpass($id){
		if ($_POST == NULL) {
			$data = array(
				'konten' => 'ubahpass',
	            'judul' => 'Ubah password',
			);
			$this->load->view('v_index', $data);
		} else {
			$pass_lama = $this->input->post('pass_lama');
			$pass_baru = $this->input->post('pass_baru');

			$cek = $this->db->query("SELECT password FROM datapelanggan where ID_pelanggan ='$id'")->row();
			if ($cek->password == $pass_lama) {
				$data = array(
					'password' => $pass_baru
				);
				$this->db->where('ID_pelanggan ', $id);
				$this->db->update('datapelanggan',$data);
				?>
				<script type="text/javascript">
					alert('Berhasil ubah password, silahkan login kembali');
					window.location='<?php echo base_url()?>app/logout';
				</script>
				<?php
			}
		}

	}

	public function login()
	{


		if ($this->input->post() == NULL) {
			$this->load->view('login');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$cek_user = $this->db->query("SELECT * FROM user WHERE username='$username' and password='$password' ");
			$cek_kry = $this->db->query("SELECT * FROM datapelanggan WHERE username='$username' and password='$password' ");
			if ($cek_user->num_rows() == 1) {
				foreach ($cek_user->result() as $row) {
					$sess_data['id_user'] = $row->id_user;
					$sess_data['nama'] = $row->nama;
					$sess_data['username'] = $row->username;
					$sess_data['level'] = 'admin';
					$this->session->set_userdata($sess_data);
				}
				redirect('app');
			} elseif ($cek_kry->num_rows() == 1) {
				foreach ($cek_kry->result() as $row) {
					$sess_data['id_user'] = $row->ID_pelanggan ;
					$sess_data['nama'] = $row->nama;
					$sess_data['username'] = $row->username;
					$sess_data['level'] = 'user';
					$this->session->set_userdata($sess_data);
				}
				redirect('app');
			} else {
				?>
				<script type="text/javascript">
					alert('Username dan password kamu salah !');
					window.location="<?php echo base_url('app/login'); ?>";
				</script>
				<?php
			}

		}
	}


	function logout()
	{
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('username');
		session_destroy();
		redirect('app/login');
	}

}
