<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pelanggan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('pelanggan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'pelanggan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pelanggan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pelanggan/index.html';
            $config['first_url'] = base_url() . 'pelanggan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->pelanggan_model->total_rows($q);
        $pelanggan = $this->pelanggan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pelanggan_data' => $pelanggan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'konten' => 'pelanggan/pelanggan_list',
            'judul' => 'Data pelanggan',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id)
    {
        $row = $this->pelanggan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'ID_pelanggan' => $row->ID_pelanggan,
		'nama' => $row->nama,
		'alamat' => $row->alamat,
		'Tanggal_bayar' => $row->Tanggal_bayar,
		'Status_bayar' => $row->Status_bayar,
        'username' => $row->username,
		'password' => $row->password,
	    );
            $this->load->view('pelanggan/pelanggan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pelanggan'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pelanggan/create_action'),
	    'ID_pelanggan' => set_value('ID_pelanggan'),
	    'nama' => set_value('nama'),
	    'alamat' => set_value('alamat'),
        'Tanggal_bayar' => set_value('Tanggal_bayar'),
	    'Status_bayar' => set_value('Status_bayar'),
        'username' => set_value('username'),
	    'password' => set_value('password'),
        'konten' => 'pelanggan/pelanggan_form',
            'judul' => 'Data pelanggan',
	);
        $this->load->view('v_index', $data);
    }
    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
        'Tanggal_bayar' => $this->input->post('Tanggal_bayar',TRUE),
		'Status_bayar' => $this->input->post('Status_bayar',TRUE),
        'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
    );

            $this->pelanggan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pelanggan'));
        }
    }
    public function update($id)
    {
        $row = $this->pelanggan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pelanggan/update_action'),
		'ID_pelanggan' => set_value('ID_pelanggan', $row->ID_pelanggan),
		'nama' => set_value('nama', $row->nama),
		'alamat' => set_value('alamat', $row->alamat),
        'Tanggal_bayar' => set_value('Tanggal_bayar', $row->Tanggal_bayar),
		'Status_bayar' => set_value('Status_bayar', $row->Status_bayar),
        'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
        'konten' => 'pelanggan/pelanggan_form',
            'judul' => 'Data pelanggan',
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pelanggan'));
        }
    }
    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('ID_pelanggan', TRUE));
        } else {
            $data = array(

		'nama' => $this->input->post('nama',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
        'Tanggal_bayar' => $this->input->post('Tanggal_bayar',TRUE),
		'Status_bayar' => $this->input->post('Status_bayar',TRUE),
        'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),

	    );

            $this->pelanggan_model->update($this->input->post('ID_pelanggan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pelanggan'));
        }
    }
    public function delete($id)
    {
        $row = $this->pelanggan_model->get_by_id($id);

        if ($row) {
            $this->pelanggan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pelanggan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pelanggan'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('Tanggal_bayar', 'Tanggal_bayar', 'trim|required');
	$this->form_validation->set_rules('Status_bayar', 'Tanggal_bayar', 'trim|required');
	$this->form_validation->set_rules('ID_pelanggan', 'ID_pelanggan', 'trim');
    $this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');

	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}