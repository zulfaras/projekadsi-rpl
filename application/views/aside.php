<aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="assets/img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>
                                <?php
                                if ($this->session->userdata('username') == 'admin') {
                                    ?>
                                    <a href="app/profiladmin/<?php echo $this->session->userdata('id_user'); ?>">
                                <?php echo $this->session->userdata('username'); ?></a>
                                    <?php
                                } else {

                                 ?>
                                <a href="app/profiluser/<?php echo $this->session->userdata('id_user'); ?>">
                                <?php echo $this->session->userdata('username'); ?></a>
                                <?php } ?>
                            </p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <?php
                        if ($this->session->userdata('level') == 'admin') {
                         ?>

                        <li class="active">
                            <a href="">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>

                        <li>
                            <a href="pelanggan">
                                <i class="fa fa-dashboard"></i> <span>Data pelanggan</span>
                            </a>
                        </li>
                        <li>
                            <a >
                                <i class="fa fa-dashboard"></i> <span>Layanan</span>
                            </a>
                        </li>
                        <li>
                            <a >
                                <i class="fa fa-dashboard"></i> <span>Data Transaksi</span>
                            </a>
                        </li>


                        <?php } elseif ($this->session->userdata('level') == 'user') {
                        $iduser = $this->session->userdata('id_user');
                        $data = $this->db->query("SELECT * FROM karyawan, gaji where karyawan.nik=gaji.nik and karyawan.id_karyawan='$iduser' order by gaji.tgl desc")->row();
                            ?>

                        <li>
                            <a href="app/tampilprofil/<?php echo $this->session->userdata('id_user'); ?>">
                                <i class="fa fa-dashboard"></i> <span>Info pengguna</span>
                            </a>
                        </li>

                        <li>
                            <a href="app/ubahpass/<?php echo $this->session->userdata('id_user'); ?>">
                                <i class="fa fa-dashboard"></i> <span> Ubah Password</span>
                            </a>
                        </li>
                        <?php } ?>


                        <li>
                            <a href="app/logout">
                                <i class="fa fa-laptop"></i> <span>LogOut</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>