<div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('pelanggan/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('pelanggan/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('pelanggan'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>

        <th>Nama</th>
        <th>Alamat</th>
        <th>Tanggal bayar</th>
        <th>Status bayar</th>
        <th>Username</th>
        <th>Password</th>
        <th>Action</th>
            </tr><?php

            if ($pelanggan_data->num_rows() > 0) {

            foreach ($pelanggan_data->result() as $pelanggan)
            {
                ?>
                <tr>
            <td width="80px"><?php echo ++$start ?></td>
            <td><?php echo $pelanggan->nama ?></td>
            <td><?php echo $pelanggan->alamat ?></td>
            <td><?php echo $pelanggan->Tanggal_bayar ?></td>
            <td><?php echo $pelanggan->Status_bayar ?></td>
            <td><?php echo $pelanggan->username ?></td>
            <td><?php echo $pelanggan->password ?></td>
            <td style="text-align:center" width="200px">
                <?php
                echo anchor(site_url('pelanggan/update/'.$pelanggan->ID_pelanggan),'Update'); 
                echo ' | ';
                echo anchor(site_url('pelanggan/delete/'.$pelanggan->ID_pelanggan),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                ?>
            </td>
        </tr>
                <?php
            }
            } else {
                ?>
                <tr>
                    <th colspan="12"><center>DATA TIDAK DI TEMUKAN</center></th>
                </tr>
                <?php
            }
            ?>
        </table>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>