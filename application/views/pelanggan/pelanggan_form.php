<form action="<?php echo $action; ?>" method="post">
        <div class="form-group">
            <label for="varchar">Nama <?php echo form_error('nama') ?></label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
        </div>
        <div class="form-group">
            <label for="alamat">Alamat <?php echo form_error('alamat') ?></label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
        </div>
        <div class="form-group">
            <label for="Tanggal_bayar">Tanggal bayar <?php echo form_error('Tanggal_bayar') ?></label>
            <input type="date" input class="form-control" value="<?= set_value('Tanggal_bayar', date('Y-m-d')); ?>" name="Tanggal_bayar" id="Tanggal_bayar" type="text" placeholder="Tanggal_bayar" />
                    <?= form_error('Tanggal_bayar'); ?>
        </div>


        <div class="form-group">
            <label for="varchar">Status_bayar <?php echo form_error('Status_bayar') ?></label>
            <?php echo $Status_bayar; ?>
            <select name="Status_bayar" class="form-control">
                <option value="">--Pilih Status bayar--</option>
                <option value="Sudah">Sudah</option>
                <option value="Belum">Belum</option>
            </select>
        </div>
        <div class="form-group">
            <label for="varchar">Username <?php echo form_error('username') ?></label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Password <?php echo form_error('password') ?></label>
            <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
        </div>

        <input type="hidden" name="ID_pelanggan" value="<?php echo $ID_pelanggan; ?>" />
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
        <a href="<?php echo site_url('pelanggan') ?>" class="btn btn-default">Cancel</a>
    </form>