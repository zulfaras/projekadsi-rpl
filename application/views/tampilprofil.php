
<h3>Data pengguna <?php echo $rw->nama; ?></h3>
<table class="table">
	<tr>
		<th>Nama</th>
		<th>:</th>
		<th><?php echo $rw->nama; ?></th>
	</tr>

	<tr>
		<th>Alamat</th>
		<th>:</th>
		<th><?php echo $rw->alamat; ?></th>
	</tr>
	<tr>
		<th>Tanggal bayar</th>
		<th>:</th>
		<th><?php echo $rw->Tanggal_bayar; ?></th>
	</tr>
	<tr>
		<th>Status bayar</th>
		<th>:</th>
		<th><?php echo $rw->Status_bayar; ?></th>
	</tr>
	<tr>
		<th>Username</th>
		<th>:</th>
		<th><?php echo $rw->username; ?></th>
	</tr>

	<tr>
		<td></td>
		<td></td>
		<td>
			<a href="app/profiluser/<?php echo $this->session->userdata('id_user'); ?>">
				<button class="btn btn-primary">Ubah Profil</button>
			</a>
			<a href="app/ubahpass/<?php echo $this->session->userdata('id_user'); ?>">
				<button class="btn btn-primary">Ubah Password</button>
			</a>
		</td>
	</tr>
</table>